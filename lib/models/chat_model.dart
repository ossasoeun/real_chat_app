class ChatModel {
  String senderChatID;
  String receiverChatID;
  String content;

  ChatModel({
    required this.senderChatID,
    required this.receiverChatID,
    required this.content,
  });
}
