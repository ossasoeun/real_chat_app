class MessageModel {
  String username;
  String message;

  MessageModel({
    required this.username,
    required this.message,
});
}

final List<MessageModel> listMessage = <MessageModel>[];