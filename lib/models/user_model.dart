class UserModel {
  String username;
  String phoneNumber;

  UserModel({
    required this.username,
    required this.phoneNumber,
});
}

final List<UserModel> friendList = [
  UserModel(username: "Cr Ronaldo", phoneNumber: "012843765"),
  UserModel(username: "Lionel Messi", phoneNumber: "015983254"),
  UserModel(username: "Mann Vanda", phoneNumber: "0978822111"),
  UserModel(username: "Preap Sovath", phoneNumber: "092907000"),
  UserModel(username: "Khat Chem", phoneNumber: "010984367"),
];