import 'package:flutter/material.dart';
import 'package:real_chat_flutter/models/user_model.dart';
import 'package:real_chat_flutter/pages/chat_page.dart';
import 'package:real_chat_flutter/pages/create_new_contact_page.dart';
import 'package:real_chat_flutter/pages/one_to_one_page.dart';
import 'package:real_chat_flutter/widgets/custom_chat_card.dart';

class AllChatPage extends StatelessWidget {
  final UserModel user;

  const AllChatPage({
    Key? key,
    required this.user,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black87,
      appBar: AppBar(
        titleSpacing: 0,
        backgroundColor: Colors.white10,
        title: const Text("Worldchat"),
        actions: [
          IconButton(
            onPressed: () => Navigator.of(context).push(
      MaterialPageRoute(
        builder: (context) => CreateNewContactPage(),
      ),
    ),
            icon: const Icon(Icons.add),
          ),
        ],
      ),
      drawer: const Drawer(),
      body: Column(
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: friendList
                .map(
                  (e) => CustomChatCard(
                    title: e.username,
                    subTitle: e.phoneNumber,
                    onTap: () => Navigator.of(context).push(
                      MaterialPageRoute(
                        builder: (context) => OneToOnePage(
                          user: e,
                          myself: user,
                        ),
                      ),
                    ),
                  ),
                )
                .toList(),
          ),
          CustomChatCard(
            title: "All people in the world",
            subTitle: "212 members playing",
            image: "https://assets.weforum.org/article/image/large_arwU0ImpJELEimgMXGmqNqWPGBvrCiwSrtbNJy_T5Qo.jpg",
            onTap: () => Navigator.of(context).push(
              MaterialPageRoute(
                builder: (context) => ChatPage(
                  user: user,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
