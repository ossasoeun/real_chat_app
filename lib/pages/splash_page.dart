import 'package:flutter/material.dart';

class SplashPage extends StatelessWidget {
  const SplashPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: Colors.white,
        alignment: Alignment.center,
        child: Image.network("https://scalebranding.com/wp-content/uploads/2021/05/fire-incognito-sb-300x300.jpg"),
      ),
    );
  }
}
