import 'package:flutter/material.dart';
import 'package:real_chat_flutter/models/chat_model.dart';
import 'package:real_chat_flutter/models/message_model.dart';
import 'package:real_chat_flutter/models/user_model.dart';
import 'package:real_chat_flutter/widgets/message.dart';
import 'package:socket_io_client/socket_io_client.dart';

class OneToOnePage extends StatefulWidget {
  final UserModel myself;
  final UserModel user;

  const OneToOnePage({Key? key, required this.user, required this.myself}) : super(key: key);

  @override
  State<OneToOnePage> createState() => _OneToOnePageState();
}

class _OneToOnePageState extends State<OneToOnePage> {
  final TextEditingController _massageCtr = TextEditingController();
  bool _isEmpty = true;

  late Socket _socket;

  final List<ChatModel> _listMessage = <ChatModel>[];

  @override
  void initState() {
    super.initState();
    _socket = io(
        'https://one2onetalk.herokuapp.com',
        OptionBuilder()
            .enableAutoConnect()
            .enableReconnection()
            .setTransports(['websocket']).setQuery({"chatID": widget.myself.phoneNumber}).build());
    _socket.onConnect((data) => debugPrint("connected"));
    _socket.onDisconnect((data) => debugPrint("disconnected"));
    _socket.on("receive_message", (data) {
      if (mounted) {
        setState(() {
        _listMessage.add(
          ChatModel(
            senderChatID: data["senderChatID"],
            receiverChatID: data["receiverChatID"],
            content: data["content"],
          ),
        );
      });
      }
    });
    _socket.connect();
  }

  // @override
  // void dispose() {
  //   _socket.disconnect();
  //   super.dispose();
  // }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black54,
      appBar: AppBar(
        titleSpacing: 0,
        backgroundColor: Colors.white10,
        title: Row(
          children: [
            Container(
              alignment: Alignment.center,
              height: 40,
              width: 40,
              decoration: const BoxDecoration(
                color: Colors.white12,
                shape: BoxShape.circle,
              ),
              child: Text(
                (widget.user.username[0] + widget.user.username[1]).toUpperCase(),
                style: const TextStyle(
                  fontSize: 16,
                ),
              ),
            ),
            const SizedBox(
              width: 10,
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  widget.user.username,
                  style: const TextStyle(
                    fontSize: 16,
                  ),
                ),
                const Text(
                  "online",
                  style: TextStyle(
                    fontSize: 12,
                    fontWeight: FontWeight.normal,
                    color: Colors.green,
                  ),
                ),
              ],
            ),
          ],
        ),
        actions: [
          IconButton(
            onPressed: () {},
            icon: const Icon(Icons.more_vert_outlined),
          ),
        ],
      ),
      body: Column(
        children: [
          Expanded(
            child: SingleChildScrollView(
              reverse: true,
              child: Padding(
                padding: const EdgeInsets.only(bottom: 10),
                child: Column(
                  children: _listMessage
                      .map(
                        (e) => MessageCard(
                          message: MessageModel(username: "sss", message: e.content),
                          isReceiver: widget.myself.phoneNumber == e.receiverChatID,
                          oneToOne: true,
                        ),
                      )
                      .toList(),
                ),
              ),
            ),
          ),
          Container(
            height: 50,
            color: Colors.white10,
            padding: const EdgeInsets.symmetric(horizontal: 15),
            child: Row(
              children: [
                const Icon(
                  Icons.emoji_emotions_outlined,
                  color: Colors.grey,
                ),
                const SizedBox(
                  width: 10,
                ),
                Expanded(
                  child: TextFormField(
                    controller: _massageCtr,
                    onFieldSubmitted: (_) => _sendMessage(),
                    onChanged: (_) {
                      if (_massageCtr.text.trim().isNotEmpty) {
                        setState(() {
                          _isEmpty = false;
                        });
                      } else {
                        setState(() {
                          _isEmpty = true;
                        });
                      }
                    },
                    style: const TextStyle(
                      color: Colors.white,
                    ),
                    decoration: const InputDecoration(
                      border: InputBorder.none,
                      hintText: "Message",
                      hintStyle: TextStyle(color: Colors.grey),
                    ),
                  ),
                ),
                const SizedBox(
                  width: 10,
                ),
                if (_isEmpty)
                  const RotationTransition(
                    turns: AlwaysStoppedAnimation(215 / 360),
                    child: Icon(
                      Icons.attach_file,
                      color: Colors.grey,
                      size: 20,
                    ),
                  ),
                if (_isEmpty)
                  const SizedBox(
                    width: 15,
                  ),
                if (_isEmpty)
                  const Icon(
                    Icons.mic_none_outlined,
                    color: Colors.grey,
                  ),
                if (!_isEmpty)
                  GestureDetector(
                    onTap: () => _sendMessage(),
                    child: const Icon(
                      Icons.send,
                      color: Colors.green,
                    ),
                  ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  void _sendMessage() {
    if (_massageCtr.text.trim().isNotEmpty) {
      setState(() {
        _listMessage.add(
          ChatModel(
            senderChatID: widget.myself.phoneNumber,
            receiverChatID: widget.user.phoneNumber,
            content: _massageCtr.text.trim(),
          ),
        );
        _isEmpty = true;
      });
      _socket.emit('send_message', {
        'content': _massageCtr.text.trim(),
        'senderChatID': widget.myself.phoneNumber,
        'receiverChatID': widget.user.phoneNumber,
      });
      _massageCtr.text = "";
    }
  }
}
