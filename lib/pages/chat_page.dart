import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:real_chat_flutter/models/message_model.dart';
import 'package:real_chat_flutter/models/user_model.dart';
import 'package:real_chat_flutter/widgets/message.dart';
import 'package:socket_io_client/socket_io_client.dart';

class ChatPage extends StatefulWidget {
  final UserModel user;

  const ChatPage({Key? key, required this.user}) : super(key: key);

  @override
  _ChatPageState createState() => _ChatPageState();
}

class _ChatPageState extends State<ChatPage> {
  final TextEditingController _massageCtr = TextEditingController();
  bool _isEmpty = true;

  final Socket _socket = io(
    "https://real-chat-1234.herokuapp.com/",
    OptionBuilder().enableAutoConnect().enableReconnection().setTransports(["websocket"]).build(),
  );

  void connect() {
    _socket.connect();
    _socket.onConnect((data) => debugPrint("connected"));
    _socket.on('receive_message', (jsonData) {
      Map<String, dynamic> data = json.decode(jsonData);
      setState(() {
        listMessage.add(
          MessageModel(
            username: data["username"],
            message: data["message"],
          ),
        );
      });
    });
    _socket.onDisconnect((data) => debugPrint("disconnected"));
  }

  @override
  void initState() {
    connect();
    super.initState();
  }

  @override
  void dispose() {
    _socket.disconnect();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black54,
      appBar: AppBar(
        titleSpacing: 0,
        backgroundColor: Colors.white10,
        title: Row(
          children: [
            Container(
              height: 40,
              width: 40,
              decoration: const BoxDecoration(
                color: Colors.white12,
                shape: BoxShape.circle,
                image: DecorationImage(
                  image: NetworkImage("https://assets.weforum.org/article/image/large_arwU0ImpJELEimgMXGmqNqWPGBvrCiwSrtbNJy_T5Qo.jpg"),
                  fit: BoxFit.cover,
                ),
              ),
            ),
            const SizedBox(
              width: 10,
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: const [
                Text(
                  "All people in the world",
                  style: TextStyle(
                    fontSize: 16,
                  ),
                ),
                Text(
                  "1352 members, 212 online",
                  style: TextStyle(
                    fontSize: 12,
                    fontWeight: FontWeight.normal,
                    color: Colors.grey,
                  ),
                ),
              ],
            ),
          ],
        ),
        actions: [
          IconButton(
            onPressed: () {},
            icon: const Icon(Icons.more_vert_outlined),
          ),
        ],
      ),
      body: Column(
        children: [
          Expanded(
            child: SingleChildScrollView(
              reverse: true,
              child: Padding(
                padding: const EdgeInsets.only(bottom: 10, left: 5),
                child: Column(
                  children: listMessage
                      .map(
                        (e) => MessageCard(
                          message: e,
                          isReceiver: e.username != widget.user.username,
                        ),
                      )
                      .toList(),
                ),
              ),
            ),
          ),
          Container(
            height: 50,
            color: Colors.white10,
            padding: const EdgeInsets.symmetric(horizontal: 15),
            child: Row(
              children: [
                const Icon(
                  Icons.emoji_emotions_outlined,
                  color: Colors.grey,
                ),
                const SizedBox(
                  width: 10,
                ),
                Expanded(
                  child: TextFormField(
                    controller: _massageCtr,
                    onFieldSubmitted: (_) => _sendMessage(),
                    onChanged: (_) {
                      if (_massageCtr.text.trim().isNotEmpty) {
                        setState(() {
                          _isEmpty = false;
                        });
                      } else {
                        setState(() {
                          _isEmpty = true;
                        });
                      }
                    },
                    style: const TextStyle(
                      color: Colors.white,
                    ),
                    decoration: const InputDecoration(
                      border: InputBorder.none,
                      hintText: "Message",
                      hintStyle: TextStyle(color: Colors.grey),
                    ),
                  ),
                ),
                const SizedBox(
                  width: 10,
                ),
                if (_isEmpty)
                  const RotationTransition(
                    turns: AlwaysStoppedAnimation(215 / 360),
                    child: Icon(
                      Icons.attach_file,
                      color: Colors.grey,
                      size: 20,
                    ),
                  ),
                if (_isEmpty)
                  const SizedBox(
                    width: 15,
                  ),
                if (_isEmpty)
                  const Icon(
                    Icons.mic_none_outlined,
                    color: Colors.grey,
                  ),
                if (!_isEmpty)
                  GestureDetector(
                    onTap: () {
                      setState(() {
                        listMessage.add(MessageModel(username: widget.user.username, message: _massageCtr.text.trim()));
                        _isEmpty = true;
                      });
                      _socket.emit(
                        "send_message",
                        json.encode(
                          {
                            "username": widget.user.username,
                            "message": _massageCtr.text.trim(),
                          },
                        ),
                      );
                      _massageCtr.text = "";
                    },
                    child: const Icon(
                      Icons.send,
                      color: Colors.green,
                    ),
                  ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  void _sendMessage() {
    if (_massageCtr.text.trim().isNotEmpty) {
      setState(() {
        listMessage.add(
          MessageModel(
            username: widget.user.username,
            message: _massageCtr.text.trim(),
          ),
        );
        _isEmpty = true;
      });
      _socket.emit(
        "send_message",
        json.encode(
          {
            "username": widget.user.username,
            "message": _massageCtr.text.trim(),
          },
        ),
      );
      _massageCtr.text = "";
    }
  }
}
