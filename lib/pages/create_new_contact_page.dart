import 'package:flutter/material.dart';
import 'package:real_chat_flutter/models/user_model.dart';
import 'package:real_chat_flutter/pages/all_chat_page.dart';
import 'package:real_chat_flutter/widgets/custom_text_field.dart';

class CreateNewContactPage extends StatelessWidget {
  final TextEditingController _usernameCtr = TextEditingController();
  final TextEditingController _phoneNumberCtr = TextEditingController();

  CreateNewContactPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black87,
      appBar: AppBar(
        backgroundColor: Colors.white10,
        title: const Text("New Contact"),
      ),
      body: Container(
        height: MediaQuery.of(context).size.height,
        padding: const EdgeInsets.symmetric(horizontal: 20),
        child: Column(
          children: [
            CustomTextField(
              controller: _usernameCtr,
              label: "Username",
            ),
            CustomTextField(
              controller: _phoneNumberCtr,
              label: "Phone Number",
              keyboardType: TextInputType.phone,
            ),
            const SizedBox(height: 10),
            Container(
              alignment: Alignment.centerRight,
              child: ElevatedButton(
                onPressed: () => _submit(context),
                child: const Text("Save"),
              ),
            ),
          ],
        ),
      ),
    );
  }

  void _submit(BuildContext context) {
    if (_usernameCtr.text.trim().length >= 2 && (_phoneNumberCtr.text.trim().length > 8 || _phoneNumberCtr.text.trim().length < 10)) {
      Navigator.of(context).pushReplacement(
        MaterialPageRoute(
          builder: (context) => AllChatPage(
            user: UserModel(
              username: _usernameCtr.text.trim(),
              phoneNumber: _phoneNumberCtr.text.trim(),
            ),
          ),
        ),
      );
    }
  }
}
