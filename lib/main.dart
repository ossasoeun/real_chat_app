
import 'package:flutter/material.dart';
import '/pages/register_page.dart';

void main() => runApp(const MyMaterial());

class MyMaterial extends StatelessWidget {
  const MyMaterial({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        primarySwatch: Colors.green,
      ),
      debugShowCheckedModeBanner: false,
      home: RegisterPage(),
    );
  }
}