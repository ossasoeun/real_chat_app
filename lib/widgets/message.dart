import 'package:flutter/material.dart';
import 'package:flutter_chat_bubble/bubble_type.dart';
import 'package:flutter_chat_bubble/chat_bubble.dart';
import 'package:flutter_chat_bubble/clippers/chat_bubble_clipper_2.dart';
import 'package:real_chat_flutter/models/message_model.dart';

class MessageCard extends StatelessWidget {
  final MessageModel message;
  final bool isReceiver;
  final bool oneToOne;

  const MessageCard({
    Key? key,
    required this.message,
    this.isReceiver = true,
    this.oneToOne = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: !isReceiver ? MainAxisAlignment.end : MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.end,
      children: [
        if (isReceiver && !oneToOne)
          SizedBox(
            height: 40,
            width: 40,
            child: CircleAvatar(
              child: Text(
                (message.username[0] + message.username[1]).toUpperCase(),
                style: const TextStyle(
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
          ),
        ChatBubble(
          elevation: 0,
          clipper: ChatBubbleClipper2(type: isReceiver ? BubbleType.receiverBubble : BubbleType.sendBubble),
          alignment: isReceiver ? Alignment.topLeft : Alignment.topRight,
          margin: const EdgeInsets.only(top: 20),
          backGroundColor: isReceiver ? Colors.white12 : Colors.green,
          child: Container(
            constraints: BoxConstraints(
              maxWidth: MediaQuery.of(context).size.width * 0.7,
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                if (isReceiver && !oneToOne)
                  Text(
                    message.username,
                    style: const TextStyle(
                      color: Colors.blueAccent,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                Text(
                  message.message,
                  style: const TextStyle(color: Colors.white),
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }
}
