import 'package:flutter/material.dart';

class CustomChatCard extends StatelessWidget {
  final String title;
  final String subTitle;
  final String? image;
  final Function()? onTap;

  const CustomChatCard({
    Key? key,
    required this.title, required this.subTitle, this.onTap, this.image
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: ListTile(
        contentPadding: const EdgeInsets.symmetric(horizontal: 5),
        leading: Container(
          height: 50,
          width: 50,
          alignment: Alignment.center,
          child: image == null? Text((title[0] + title[1]).toUpperCase(), style: const TextStyle(
            color: Colors.white,
            fontSize: 18,
            fontWeight: FontWeight.bold,
          ),): const SizedBox(),
          decoration: BoxDecoration(
            shape: BoxShape.circle,
            color: Colors.white12,
            image: image != null? DecorationImage(
              image: NetworkImage(image!),
              fit: BoxFit.cover,
            ): null,
          ),
        ),
        title: Text(
          title,
          style: const TextStyle(
            fontSize: 16.0,
            color: Colors.white,
            fontWeight: FontWeight.bold,
          ),
        ),
        subtitle: Text(
          subTitle,
          style: const TextStyle(
            fontSize: 13.0,
            color: Colors.grey,
          ),
        ),
        trailing: const Icon(Icons.navigate_next, color: Colors.white,),
      ),
    );
  }
}