import 'package:flutter/material.dart';

class CustomTextField extends StatelessWidget {
  final TextEditingController controller;
  final String label;
  final TextInputType? keyboardType;

  const CustomTextField({
    Key? key,
    required this.controller,
    required this.label,
    this.keyboardType,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      // onFieldSubmitted: (str) => _submit(context),
      controller: controller,
      style: const TextStyle(
        color: Colors.white,
      ),
      keyboardType: keyboardType,
      decoration: InputDecoration(
        label: Text(label, style: const TextStyle(color: Colors.grey)),
        border: const UnderlineInputBorder(borderSide: BorderSide(color: Colors.white)),
        enabledBorder: const UnderlineInputBorder(borderSide: BorderSide(color: Colors.white)),
        // suffixIcon: GestureDetector(
        //   onTap: () => _submit(context),
        //   child: const Icon(Icons.send),
        // ),
      ),
    );
  }
}
